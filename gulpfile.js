"use strict";

var gulp         = require('gulp'),
    connect      = require('gulp-connect'),
    opn          = require('opn'),
    wiredep      = require('wiredep').stream,
    notify       = require("gulp-notify"),
    jade         = require('gulp-jade'),
    sass         = require('gulp-sass'),
    uncss        = require('gulp-uncss'),
    spritesmith  = require("gulp.spritesmith"),
    htmlhint     = require("gulp-htmlhint"),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber      = require('gulp-plumber'),
    useref       = require('gulp-useref'),
    uglify       = require('gulp-uglify'),
    minifyCss    = require('gulp-minify-css'),
    gulpif       = require('gulp-if'),
    imagemin     = require('gulp-imagemin'),
    changed      = require('gulp-changed'),
    browserSync  = require("browser-sync").create();
    

//Server
//gulp.task('connect', function() {
//    connect.server({
//        root: 'app',
//        livereload: true
//    });
//    opn('http://localhost:8080/');
//});

//Jade
gulp.task('jade', function(){
    gulp.src('src/jade/_pages/*.jade')
        .pipe(changed('public',{
            extension: '.html'
        }))
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(jade({pretty:true}))
        .pipe(htmlhint())
        .pipe(htmlhint.reporter())
        .pipe(gulp.dest('public'))
        .pipe(notify('HTML was changed'))
        .pipe(browserSync.stream());
      
  });


//Sass
gulp.task('sass', function () {
    gulp.src('src/css/scss/*.scss')
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sass())
        .pipe(autoprefixer({ browsers: ['last 3 versions'] }))
        .pipe(gulp.dest('public/css'))
        .pipe(notify('CSS was changed'))
        .pipe(browserSync.stream());
        
});


//js
gulp.task('js', function(){
    gulp.src('src/js/*.js')
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(gulp.dest('public/js'))
    .pipe(notify('JS was changed!'))
    .pipe(browserSync.stream());
});



//image
gulp.task('img', function() {
    gulp.src('src/img/*.*')
        .pipe(changed('public/images')) 
        .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest('public/images'))
        .pipe(notify('Picture was added'))
        .pipe(browserSync.stream());
});

//fonts
gulp.task('fonts', function() {
    gulp.src('src/fonts/*.*')
        .pipe(changed('public/fonts'))
        .pipe(gulp.dest('public/fonts'))
        .pipe(notify('Fonts was added'))
        .pipe(browserSync.stream());
});

//Слежка
gulp.task('serve', ['jade','sass','js','bower','img','fonts'], function(){
     browserSync.init({
        server: {
            baseDir: "public"
        }
    });
    gulp.watch(['src/js/*.js'],['js']);
    gulp.watch(['src/jade/*/*.jade'],['jade']);
    gulp.watch(['src/css/scss/*.scss'],['sass']);
    gulp.watch(['src/img/*.*'],['img']);
    gulp.watch(['src/fonts/*.*'],['fonts']);
    gulp.watch('bower.json',['bower']);
    
});

//Спрайты
gulp.task('sprite', function() {
    var spriteData =
        gulp.src('./src/images/sprite/*.*') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.css',
                padding: 25
            }));
    spriteData.img.pipe(gulp.dest('./src/images/build/images/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('./src/images/build/styles/')); // путь, куда сохраняем стили
});

//Подключаем ссылки на bower_components
gulp.task('bower', function(){
    gulp.src('src/jade/_layout/*.jade')
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(wiredep({
        directory: 'public/bower_components'
    }))
    .pipe(gulp.dest('src/jade/_layout/'))
    .pipe(notify('BOWER was changed'))
    .pipe(browserSync.stream());
});


//Сборка
gulp.task('build', function(){
    var assets = useref.assets();
    return gulp.src('public/*.html')
        .pipe(assets)
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('./dist'))
        .pipe(notify('Билд успешно собран!'));
})

//Default
gulp.task('default',['serve']);